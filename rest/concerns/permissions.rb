module Rest
  module Concerns
    module Permissions
      extend ActiveSupport::Concern
      included do
        def can?(action_name)
          send("can_#{action_name}?")
        end

        def can_manage?
          false
        end

        def can_read?
          false
        end

        protected

        def can_index?
          can_read? || can_manage?
        end

        def can_total?
          can_read? || can_manage?
        end

        def can_where_count?
          can_read? || can_manage?
        end

        def can_show?
          can_read? || can_manage?
        end
        def can_create?
          can_manage?
        end

        def can_update?
          can_manage?
        end

        def can_upsert?
          can_manage?
        end

        def can_destroy?
          can_manage?
        end

        def can_new?
          can_read? || can_manage?
        end

        def can_find_by?
          can_read? || can_manage?
        end

        def can_find_or_initialize_by?
          can_read? || can_manage?
        end

        def can_find_or_create_by?
          can_manage?
        end

        def admin_or_super?
          super_user? || permissions['admin'].present?
        end

        def super_user?
          permissions['super-user'].present?
        end

        def permissions
          @permissions ||= session['permissions'] || {}
        end
      end
    end
  end
end
