module Rest
  module Concerns
    module Session
      extend ActiveSupport::Concern
      included do
=begin
        def current_user
          return nil if session.nil? || session['user_id'].nil?
          return OpenStruct.new(id: session['user_id']) unless defined?(User)
          @current_user ||= Cqrs::Users::User.find_by(id: session['user_id'])
          return nil if @current_user.nil?
          @current_user
        end

        def session
          return @session unless @session.nil?
          return if token.nil?
          return if redis_readonly.nil? || session_string.blank?
          @session ||= JSON.parse(session_string)&.with_indifferent_access
        end
=end

        protected

        def session_string
          @session_string ||= redis_readonly.get(token)
        end

        private

        def organization_id
          @organization_id ||= request.headers['X-Organization-Id'] || params[:organization_id]
        end

        def token
          @token ||=  request.headers['X-Session-Id'] || request.headers['Session-Id']
          return @token unless @token.nil?
          authenticate_with_http_token do |token, options|
            @token = token
          end
          return @token
        end

        def redis_readonly
          @redis_readonly ||= Redis.new(Rails.application.config_for(:redis_readonly))
        end
      end
    end
  end
end
